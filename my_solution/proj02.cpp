/**
 * @file        proj02.cpp
 * @author      Jiri Jaros, Radek Hrbacek, Filip Vaverka and Vojtech Nikl\n
 *              Faculty of Information Technology\n
 *              Brno University of Technology\n
 *              jarosjir@fit.vutbr.cz
 *
 * @brief       Parallelisation of Heat Distribution Method in Heterogenous
 *              Media using MPI and OpenMP
 *
 * @version     2017
 * @date        10 April 2015, 10:22 (created)\n
 *              28 March 2017, 12:02 (revised)
 *
 * @detail
 * This is the main file of the project. Add all code here.
 */


#include <mpi.h>
#include <omp.h>

#include <string.h>
#include <string>
#include <cmath>

#include <hdf5.h>

#include <sstream>
#include <iostream>
#include <immintrin.h>

#include "MaterialProperties.h"
#include "BasicRoutines.h"


using namespace std;


//----------------------------------------------------------------------------//
//---------------------------- Global variables ------------------------------//
//----------------------------------------------------------------------------//

/// Temperature data for sequential version.
float *seqResult = NULL;
/// Temperature data for parallel method.
float *parResult = NULL;

/// Parameters of the simulation
TParameters parameters;

/// Material properties
TMaterialProperties materialProperties;

//----------------------------------------------------------------------------//
//------------------------- Function declarations ----------------------------//
//----------------------------------------------------------------------------//

/// Sequential implementation of the Heat distribution
void SequentialHeatDistribution(float                     *seqResult,
                                const TMaterialProperties &materialProperties,
                                const TParameters         &parameters,
                                string                     outputFileName);

/// Parallel Implementation of the Heat distribution (Non-overlapped file output)
void ParallelHeatDistribution(float                     *parResult,
                              const TMaterialProperties &materialProperties,
                              const TParameters         &parameters,
                              string                     outputFileName);

/// Store time step into output file
void StoreDataIntoFile(hid_t         h5fileId,
                       const float * data,
                       const size_t  edgeSize,
                       const size_t  snapshotId,
                       const size_t  iteration);

/// Store time step into output file using parallel HDF5
void StoreDataIntoFileParallel(hid_t h5fileId,
                               const float *data,
                               const size_t edgeSize,
                               const size_t tileWidth, const size_t tileHeight,
                               const size_t tilePosX, const size_t tilePosY,
                               const size_t snapshotId,
                               const size_t iteration);


//----------------------------------------------------------------------------//
//------------------------- Function implementation  -------------------------//
//----------------------------------------------------------------------------//


void ComputePoint(float  *oldTemp,
                  float  *newTemp,
                  float  *params,
                  int    *map,
                  size_t  i,
                  size_t  j,
                  size_t  edgeSize,
                  float   airFlowRate,
                  float   coolerTemp)
{
  // [i] Calculate neighbor indices
  const int center    = i * edgeSize + j;
  const int top[2]    = { center - (int)edgeSize, center - 2*(int)edgeSize };
  const int bottom[2] = { center + (int)edgeSize, center + 2*(int)edgeSize };
  const int left[2]   = { center - 1, center - 2};
  const int right[2]  = { center + 1, center + 2};

  // [ii] The reciprocal value of the sum of domain parameters for normalization
  const float frac = 1.0f / (params[top[0]]    + params[top[1]]    +
                             params[bottom[0]] + params[bottom[1]] +
                             params[left[0]]   + params[left[1]]   +
                             params[right[0]]  + params[right[1]]  +
                             params[center]);

  // [iii] Calculate new temperature in the grid point
  float pointTemp = 
        oldTemp[top[0]]    * params[top[0]]    * frac +
        oldTemp[top[1]]    * params[top[1]]    * frac +
        oldTemp[bottom[0]] * params[bottom[0]] * frac +
        oldTemp[bottom[1]] * params[bottom[1]] * frac +
        oldTemp[left[0]]   * params[left[0]]   * frac +
        oldTemp[left[1]]   * params[left[1]]   * frac +
        oldTemp[right[0]]  * params[right[0]]  * frac +
        oldTemp[right[1]]  * params[right[1]]  * frac +
        oldTemp[center]    * params[center]    * frac;

  // [iv] Remove some of the heat due to air flow (5% of the new air)
  pointTemp = (map[center] == 0)
              ? (airFlowRate * coolerTemp) + ((1.0f - airFlowRate) * pointTemp)
              : pointTemp;

  newTemp[center] = pointTemp;
}

/**
 * Sequential version of the Heat distribution in heterogenous 2D medium
 * @param [out] seqResult          - Final heat distribution
 * @param [in]  materialProperties - Material properties
 * @param [in]  parameters         - parameters of the simulation
 * @param [in]  outputFileName     - Output file name (if NULL string, do not store)
 *
 */
void SequentialHeatDistribution(float                      *seqResult,
                                const TMaterialProperties &materialProperties,
                                const TParameters         &parameters,
                                string                     outputFileName)
{
  // [1] Create a new output hdf5 file
  hid_t file_id = H5I_INVALID_HID;
  
  if (outputFileName != "")
  {
    if (outputFileName.find(".h5") == string::npos)
      outputFileName.append("_seq.h5");
    else
      outputFileName.insert(outputFileName.find_last_of("."), "_seq");
    
    file_id = H5Fcreate(outputFileName.c_str(),
                        H5F_ACC_TRUNC,
                        H5P_DEFAULT,
                        H5P_DEFAULT);
    if (file_id < 0) ios::failure("Cannot create output file");
  }


  // [2] A temporary array is needed to prevent mixing of data form step t and t+1
  float *tempArray = (float *)_mm_malloc(materialProperties.nGridPoints * 
                                         sizeof(float), DATA_ALIGNMENT);

  // [3] Init arrays
  for (size_t i = 0; i < materialProperties.nGridPoints; i++)
  {
    tempArray[i] = materialProperties.initTemp[i];
    seqResult[i] = materialProperties.initTemp[i];
  }

  // [4] t+1 values, t values
  float *newTemp = seqResult;
  float *oldTemp = tempArray;

  if (!parameters.batchMode) 
    printf("Starting sequential simulation... \n");
  
  //---------------------- [5] start the stop watch ------------------------------//
  double elapsedTime = MPI_Wtime();
  size_t i, j;
  size_t iteration;
  float middleColAvgTemp = 0.0f;
  size_t printCounter = 1;

  // [6] Start the iterative simulation
  for (iteration = 0; iteration < parameters.nIterations; iteration++)
  {
    // [a] calculate one iteration of the heat distribution (skip the grid points at the edges)
    for (i = 2; i < materialProperties.edgeSize - 2; i++)
      for (j = 2; j < materialProperties.edgeSize - 2; j++)
        ComputePoint(oldTemp,
                     newTemp,
                     materialProperties.domainParams,
                     materialProperties.domainMap,
                     i, j,
                     materialProperties.edgeSize, 
                     parameters.airFlowRate,
                     materialProperties.coolerTemp);

    // [b] Compute the average temperature in the middle column
    middleColAvgTemp = 0.0f;
    for (i = 0; i < materialProperties.edgeSize; i++)
      middleColAvgTemp += newTemp[i*materialProperties.edgeSize +
                          materialProperties.edgeSize/2];
    middleColAvgTemp /= materialProperties.edgeSize;

    // [c] Store time step in the output file if necessary
    if ((file_id != H5I_INVALID_HID)  && ((iteration % parameters.diskWriteIntensity) == 0))
    {
      StoreDataIntoFile(file_id,
                        newTemp,
                        materialProperties.edgeSize,
                        iteration / parameters.diskWriteIntensity,
                        iteration);
    }

    // [d] Swap new and old values
    swap(newTemp, oldTemp);

    // [e] Print progress and average temperature of the middle column
    if ( ((float)(iteration) >= (parameters.nIterations-1) / 10.0f * (float)printCounter) 
        && !parameters.batchMode)
    {
      printf("Progress %ld%% (Average Temperature %.2f degrees)\n", 
             (iteration+1) * 100L / (parameters.nIterations), 
             middleColAvgTemp);
      ++printCounter;
    }
  } // for iteration

  //-------------------- stop the stop watch  --------------------------------//  
  double totalTime = MPI_Wtime() - elapsedTime;

  // [7] Print final result
  if (!parameters.batchMode)
    printf("\nExecution time of sequential version %.5f\n", totalTime);
  else
    printf("%s;%s;%f;%e;%e\n", outputFileName.c_str(), "seq",
                               middleColAvgTemp, totalTime,
                               totalTime / parameters.nIterations);   

  // Close the output file
  if (file_id != H5I_INVALID_HID) H5Fclose(file_id);

  // [8] Return correct results in the correct array
  if (iteration & 1)
    memcpy(seqResult, tempArray, materialProperties.nGridPoints * sizeof(float));

  _mm_free(tempArray);
} // end of SequentialHeatDistribution
//------------------------------------------------------------------------------


/**
 * Parallel version of the Heat distribution in heterogenous 2D medium
 * @param [out] parResult          - Final heat distribution
 * @param [in]  materialProperties - Material properties
 * @param [in]  parameters         - parameters of the simulation
 * @param [in]  outputFileName     - Output file name (if NULL string, do not store)
 *
 * @note This is the function that students should implement.                                                  
 */
void ParallelHeatDistribution(float                     *parResult,
                              const TMaterialProperties &materialProperties,
                              const TParameters         &parameters,
                              string                     outputFileName)
{
  // Get MPI rank and size
  int rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  
  hid_t file_id = H5I_INVALID_HID;
  
  if(!parameters.useParallelIO)
  {
      // Serial I/O
      if(rank == 0 && outputFileName != "")
      {
          if(outputFileName.find(".h5") == string::npos)
              outputFileName.append("_par.h5");
          else
              outputFileName.insert(outputFileName.find_last_of("."), "_par");

          file_id = H5Fcreate(outputFileName.c_str(),
                              H5F_ACC_TRUNC,
                              H5P_DEFAULT,
                              H5P_DEFAULT);
          if(file_id < 0) ios::failure("Cannot create output file");
      }
  }
  else
  {
      // Parallel I/O
      if(outputFileName != "")
      {
          if(outputFileName.find(".h5") == string::npos)
              outputFileName.append("_par.h5");
          else
              outputFileName.insert(outputFileName.find_last_of("."), "_par");

          hid_t hPropList = H5Pcreate(H5P_FILE_ACCESS);
          H5Pset_fapl_mpio(hPropList, MPI_COMM_WORLD, MPI_INFO_NULL);

          file_id = H5Fcreate(outputFileName.c_str(),
                              H5F_ACC_TRUNC,
                              H5P_DEFAULT,
                              hPropList);
          H5Pclose(hPropList);
          if(file_id < 0) ios::failure("Cannot create output file");
      }
  }
  
  
  //--------------------------------------------------------------------------//
  //---------------- THE SECTION WHERE STUDENTS MAY ADD CODE -----------------//
  //--------------------------------------------------------------------------//
    
    /*************** vypočítáme rozdělení domény na dlaždice a toto rozdělení rozešleme ostatním procesům, stejně tak jim pošleme další parametry simulace *********/
    
    // lokální proměnné pro odpovídající atributy objektů materialProperties a parameters
    float airFlowRate;
    float coolerTemp;
    size_t diskWriteIntensity;
    size_t nIterations;
    size_t edgeSize;
    bool useParallelIO;
    bool batchMode;
    
    int numOfTilesPerRow; // počet dlaždic na řádku
    int numOfTilesPerCol; // počet dlaždic ve sloupci
    int widthOfTile;  // šířka dlaždice v počtech bodů mřížky diskretizovsaného prostoru
    int heightOfTile; // výška dlaždice v počtech bodů mřížky diskretizovsaného prostoru

    // proces s rankem 0 vypočítá parametry rozdělení domény na dlaždice a uloží si parametry simulace do lok. proměnných
    if (rank == 0) {
        coolerTemp = materialProperties.coolerTemp;
        airFlowRate = parameters.airFlowRate;
        diskWriteIntensity = parameters.diskWriteIntensity;
        nIterations = parameters.nIterations;
        edgeSize = materialProperties.edgeSize;
        useParallelIO = parameters.useParallelIO;
        batchMode = parameters.batchMode;

        // počet procesů je sudá mocnina dvou, budeme dělit doménu na čtvercové dlaždice       
        int power = (int) log2((double) size);
        if (power % 2 == 0) {
            widthOfTile = heightOfTile = materialProperties.edgeSize / (int) sqrt(size);
            numOfTilesPerRow = numOfTilesPerCol = materialProperties.edgeSize / heightOfTile;
        }
        // počet procesů je lichá mocnina dvou, budeme dělit doménu na obdélníkové dlaždice  
        else {
            // většina proměnných jsou shared, pokud do nějakých shared proměnných ukládáme, tak je zaručeno že toto ukládání provede jen jedno vlákno
            // jedna proměnná je firstPrivate (kvůli vyššímu výkonu při přístpu k této proměnné)
            #pragma omp parallel for default(none) shared(widthOfTile, heightOfTile, numOfTilesPerRow, numOfTilesPerCol, materialProperties) firstprivate(size)
            for (int i = 2; i <= materialProperties.edgeSize; i++) {
                int sizeOfPotentialTile = i * (i / 2);
                // pokud je splněna podmínka (to se stane právě jedenkrát za celou dobu běhu cyklu) tak je vhodné pomocí breka přerušit cyklus, nicméně toto v OpenMP nemůžeme, prot zde break není - což vadí v případě nehybridního použití programu, kde bychom to mohli detekovat pomocí nějaké podmínky na počet vláken např. teoreticky, prakticky to nebudem komplikovat, ale víme o tomto "prooblému"
                if (materialProperties.edgeSize * materialProperties.edgeSize == sizeOfPotentialTile * size) {
                    
                    widthOfTile = i;
                    heightOfTile = i / 2;
                    numOfTilesPerRow = materialProperties.edgeSize / widthOfTile;
                    numOfTilesPerCol = materialProperties.edgeSize / heightOfTile;
                }   
            } 
            
        }
        
        // vypíšeme si nějaké info o rozděleni domény na dlaždice
        if (!parameters.batchMode) {
            printf("Tile parameters: \n width: %d \n height: %d \n tiles per row: %d \n tiles per column: %d \n\n", widthOfTile, heightOfTile, numOfTilesPerRow, numOfTilesPerCol);   
        }
    }

    // rozešleme peocesem s rankem 0 broadcastem ostatním procesům parametry simulace a parametry rozdělení domény na dlaždice
    MPI_Bcast(&airFlowRate, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&coolerTemp, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&diskWriteIntensity, 1, MPI_UNSIGNED_LONG_LONG, 0, MPI_COMM_WORLD); // diskWriteIntensity je typu size_t, takže pro jistotu pošleme s co největším rozsahem MPI_UNSIGNED_LONG_LONG 
    MPI_Bcast(&nIterations, 1, MPI_UNSIGNED_LONG_LONG, 0, MPI_COMM_WORLD); // nIterations je typu size_t, takže pro jistotu pošleme s co největším rozsahem MPI_UNSIGNED_LONG_LONG 
    MPI_Bcast(&edgeSize, 1, MPI_UNSIGNED_LONG_LONG, 0, MPI_COMM_WORLD); // nIterations je typu size_t, takže pro jistotu pošleme s co největším rozsahem MPI_UNSIGNED_LONG_LONG 
    MPI_Bcast(&useParallelIO, 1, MPI_INT, 0, MPI_COMM_WORLD); // konverze bool na int a naopak
    MPI_Bcast(&batchMode, 1, MPI_INT, 0, MPI_COMM_WORLD); // konverze bool na int a naopak
    
    MPI_Bcast(&numOfTilesPerRow, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&numOfTilesPerCol, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&widthOfTile, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&heightOfTile, 1, MPI_INT, 0, MPI_COMM_WORLD);
    
    
    size_t sizeOfTile = widthOfTile * heightOfTile; // počet bodů v dlaždici 
    
    /*************** rozešleme procesům dlaždice procesem s rankem 0 (dlaždice s hodnotami domény, ale také s parametry, aj.) *********/
    
    // dlaždice přijaté od master procesu 0, bude ovšem potřeba je zvětšit, abychom měli kde uložit halo zóny  
    float *tileOfnewTemp = (float *)_mm_malloc(widthOfTile * heightOfTile * sizeof(float), DATA_ALIGNMENT);
    float *tileOfoldTemp = (float *)_mm_malloc(widthOfTile * heightOfTile * sizeof(float), DATA_ALIGNMENT);
    int *tileOfdomainMap = (int *)_mm_malloc(widthOfTile * heightOfTile * sizeof(int), DATA_ALIGNMENT);
    float *tileOfdomainParams = (float *)_mm_malloc(widthOfTile * heightOfTile * sizeof(float), DATA_ALIGNMENT);
        
    MPI_Datatype tmpType; // dočasný datový typ, který ještě není resized
    MPI_Datatype tileOfInitTempType;     // resized typ dlaždice pole materialProperties.InitTemp
    MPI_Datatype tileOfdomainMapType;    // resized typ dlaždice pole materialProperties.MapType
    MPI_Datatype tileOfdomainParamsType; // resized typ dlaždice pole materialProperties.ParamsType
    
    // pomocné pole pro získání dlaždice
    const int array_of_sizes[2] = {edgeSize, edgeSize};   
    const int array_of_subsizes[2] = {heightOfTile, widthOfTile};   
    const int array_of_starts[2] = {0, 0};
    
    // dlaždice pole materialProperties.InitTemp, kterou resizujeme na její šířku
    MPI_Type_create_subarray(2, array_of_sizes, array_of_subsizes, array_of_starts, MPI_ORDER_C, MPI_FLOAT, &tmpType);
    MPI_Type_create_resized(tmpType, 0, widthOfTile*sizeof(float), &tileOfInitTempType);
    MPI_Type_commit(&tileOfInitTempType);
    MPI_Type_free(&tmpType);
    
    // dlaždice pole materialProperties.MapType, kterou resizujeme na její šířku
    MPI_Type_create_subarray(2, array_of_sizes, array_of_subsizes, array_of_starts, MPI_ORDER_C, MPI_INT, &tmpType);
    MPI_Type_create_resized(tmpType, 0, widthOfTile*sizeof(int), &tileOfdomainMapType);
    MPI_Type_commit(&tileOfdomainMapType);
    MPI_Type_free(&tmpType);
    
    // dlaždice pole materialProperties.ParamsType, kterou resizujeme na její šířku
    MPI_Type_create_subarray(2, array_of_sizes, array_of_subsizes, array_of_starts, MPI_ORDER_C, MPI_FLOAT, &tmpType);
    MPI_Type_create_resized(tmpType, 0, widthOfTile*sizeof(float), &tileOfdomainParamsType);
    MPI_Type_commit(&tileOfdomainParamsType);
    MPI_Type_free(&tmpType);
    
    int sendcounts[size]; // v našem případě pole samých 1 - každému procesoru se pošle jedna dlaždice
    int displs[size]; // pole posunů jednotlivých dlaždic vůči doméně
    
    // firstprivatizujeme cokoliv jen jde (až na typy, které nejsou prosté a jejichž hodnoty by se špatně kopírovali, nebo pole které nejsou dynamicky alokována) kvůli vysokému výkonu při přístupu k dané proměnné
    // celé toto provádí vlákna parlaleně, protože jsou zde for smyčky blízko sebe a opakované startování parlalení sekce pro každou for smyčku by asi byla zbytečná režie
    #pragma omp parallel default(none) shared(sendcounts, displs, materialProperties, tileOfInitTempType, tileOfoldTemp, tileOfnewTemp) firstprivate(size, numOfTilesPerCol, numOfTilesPerRow, widthOfTile, heightOfTile, sizeOfTile) 
    {
        // tohle má smysl jen u procesu s rankem 0, ale ničemu nevadí, když to udělají všechny procesy, stejně by museli na proces s rankem 0 čekat, tak ať se nenudí, práce šlechtí
        #pragma omp for simd
        for (int i=0; i<size; i++) {
            sendcounts[i] = 1;
        }
        // vypočítáme pole posunů
        // tohle má smysl jen u procesu s rankem 0, ale ničemu nevadí, když to udělají všechny procesy, stejně by museli na procesor s rankem 0 čekat, tak ať se nenudí, práce šlechtí
        #pragma omp for simd
        for (int i=0; i < numOfTilesPerCol; i++) {
            int displsForCurrentRow = heightOfTile * numOfTilesPerRow * i;
            for (int j=0; j < numOfTilesPerRow; j++) {
                displs[i * numOfTilesPerRow + j] = displsForCurrentRow++;
            }
        }
        // předpokládáme FUNNELED thread safety
        #pragma omp master 
        {
            // rozešleme dlaždice pole materialPropertiOfnewTempes.InitTemp odpovídajícím procesům      
            MPI_Scatterv(materialProperties.initTemp, sendcounts, displs, tileOfInitTempType, 
                        tileOfoldTemp, widthOfTile * heightOfTile, MPI_FLOAT, 0, MPI_COMM_WORLD);
        }
        // sesynchronizujem tym vlaken, aby vsechna vlakna videla prijate hodnoty v tileOfoldTemp
        #pragma omp barrier
        
        // přijaté dlaždice pole materialProperties.InitTemp si procesy uloží i do pole nově vypočítaných hodnot tileOfnewTemp             
        #pragma omp for simd
        for (size_t i = 0; i < sizeOfTile; i++) {
            tileOfnewTemp[i] = tileOfoldTemp[i];
        }
    }
    
    // rozešleme dlaždice pole materialProperties.ParamsType odpovídajícím procesům              
    MPI_Scatterv(materialProperties.domainParams, sendcounts, displs, tileOfdomainParamsType, 
                tileOfdomainParams, widthOfTile * heightOfTile, MPI_FLOAT, 0, MPI_COMM_WORLD);
        
    // rozešleme dlaždice pole materialProperties.MapType odpovídajícím procesům  
    MPI_Scatterv(materialProperties.domainMap, sendcounts, displs, tileOfdomainMapType, 
                tileOfdomainMap, widthOfTile * heightOfTile, MPI_INT, 0, MPI_COMM_WORLD);

    /*************** procesy přijali své dlaždice, nyní si tyto dlaždice rozšíří o haló zóny - tedy přijaté dlaždice se nakopírují do větších dlaždic, kde bude místo i na halo zony*********/        
    
    int widthOfExtendedTile = widthOfTile + 4;   // šířka rozšířené dlaždice o haló zóny
    int heightOfExtendedTile = heightOfTile + 4; // výška rozšířené dlaždice o haló zóny
                           
    // rozšířené dlaždice přijaté od master procesu 0 o haló zóny
    float *extendedTileOfnewTemp = (float *)_mm_malloc(widthOfExtendedTile * heightOfExtendedTile * sizeof(float), DATA_ALIGNMENT);
    float *extendedTileOfoldTemp = (float *)_mm_malloc(widthOfExtendedTile * heightOfExtendedTile * sizeof(float), DATA_ALIGNMENT);
    int *extendedTileOfdomainMap = (int *)_mm_malloc(widthOfExtendedTile * heightOfExtendedTile * sizeof(int), DATA_ALIGNMENT);
    float *extendedTileOfdomainParams = (float *)_mm_malloc(widthOfExtendedTile * heightOfExtendedTile * sizeof(float), DATA_ALIGNMENT);
        
    // do rozšířených dlaždic uložíme přijaté originální dlaždice
    // firstprivatizujeme cokoliv jen jde kvůli výkonu přístupu k daným proměnným, respektive ukazatelům
    #pragma omp parallel for simd default(none) firstprivate(extendedTileOfnewTemp, extendedTileOfoldTemp, extendedTileOfdomainMap, extendedTileOfdomainParams, heightOfExtendedTile, widthOfExtendedTile, tileOfnewTemp, tileOfoldTemp, tileOfdomainMap, tileOfdomainParams, widthOfTile)
    for (int i = 2; i < heightOfExtendedTile - 2; i++) {
        for (int j = 2; j < widthOfExtendedTile - 2; j++) {
            int extendedIndex = i * widthOfExtendedTile + j; // index v rozšířené dlaždici
            int originIndex = (i-2) * widthOfTile + (j - 2); // index v původní dlaždici 
            extendedTileOfnewTemp[extendedIndex] = tileOfnewTemp[originIndex];
            extendedTileOfoldTemp[extendedIndex] = tileOfoldTemp[originIndex];
            extendedTileOfdomainMap[extendedIndex] = tileOfdomainMap[originIndex];
            extendedTileOfdomainParams[extendedIndex] = tileOfdomainParams[originIndex];
        }   
    }   
        
        
    /*************** PRITETI SOUSEDNICH HODNOT DO HALO OBLASTI EXTENDED DLAZDIC PRI INICIALIZACI *********/        
    
    // definujme datovy typ pro dva sousedni sloupce typu int v extended dlazdici (predstavujici dilci halo zonu)       
    MPI_Datatype twoIntAdjacentColumns;
    MPI_Type_vector(heightOfTile, 2, widthOfExtendedTile, MPI_INT, &twoIntAdjacentColumns);
    MPI_Type_commit(&twoIntAdjacentColumns);
    
    // definujme datovy typ pro dva sousedni sloupce typu float v extended dlazdici (predstavujici dilci halo zonu)       
    MPI_Datatype twoFloatAdjacentColumns;
    MPI_Type_vector(heightOfTile, 2, widthOfExtendedTile, MPI_FLOAT, &twoFloatAdjacentColumns);
    MPI_Type_commit(&twoFloatAdjacentColumns);
    
    // definujme datovy typ pro dva sousedni radky typu int v extended dlazdici (predstavujici dilci halo zonu)       
    MPI_Datatype twoIntAdjacentRows;
    MPI_Type_vector(2, widthOfTile, widthOfExtendedTile, MPI_INT, &twoIntAdjacentRows);
    MPI_Type_commit(&twoIntAdjacentRows);
    
    // definujme datovy typ pro dva sousedni radky typu float v extended dlazdici (predstavujici dilci halo zonu)       
    MPI_Datatype twoFloatAdjacentRows;
    MPI_Type_vector(2, widthOfTile, widthOfExtendedTile, MPI_FLOAT, &twoFloatAdjacentRows);
    MPI_Type_commit(&twoFloatAdjacentRows);
    
    // poznamenáme si, jestli má naše dlaždice sousedy
    bool hasUpperNeighbor = (rank < numOfTilesPerRow) ? false : true;
    bool hasBottomNeighbor = (rank >= (numOfTilesPerCol - 1) * numOfTilesPerRow) ? false : true;
    bool hasLeftNeighbor = (rank % numOfTilesPerRow == 0) ? false : true;
    bool hasRightNeighbor = (rank % numOfTilesPerRow == numOfTilesPerRow - 1) ? false : true;
    
    // poznamenáme si, jaké má naše dlaždice sousedy (může nám při počítání vyjít hloupost, pokud dlaždice nemá příslušného souseda)
    int rankOfUpperNeighbor = rank - numOfTilesPerRow;
    int rankOfBottomNeighbor = rank + numOfTilesPerRow;
    int rankOfLeftNeighbor = rank - 1;
    int rankOfRightNeighbor = rank + 1;
    
    int TAG = 0; // Obecný tag, používaný pro veškerou komunikaci  

    int numOfRequest = 0; // kolik bude celkově použito reuestů pro point to point komunikaci, záleží kolik má dlaždice sousedů
    if (hasUpperNeighbor) numOfRequest += 6; // s každým sousedem si vyměníme celkově 6 zpráv = 6 requestů
    if (hasBottomNeighbor) numOfRequest += 6;
    if (hasLeftNeighbor) numOfRequest += 6;
    if (hasRightNeighbor) numOfRequest += 6;
    
    MPI_Request array_of_requests[numOfRequest]; // pole requestů
    MPI_Status array_of_statuses[numOfRequest]; // pole statusů
    

    int offsetOfHaloZoneToSendToUpper = widthOfExtendedTile * 2 + 2; // offset kde začíná haló zóna, kterou je třeba odeslat hornímu sousedovi
    int offsetOfHaloZoneToReceiveFromUpper = 2;                      // offset kde začíná haló zóna, kterou je třeba přijmout od horního souseda

    int offsetOfHaloZoneToSendToBottom = ((heightOfExtendedTile - 4) * widthOfExtendedTile) + 2; // offset kde začíná haló zóna, kterou je třeba odeslat spodnímu sousedovi
    int offsetOfHaloZoneToReceiveFromBottom = ((heightOfExtendedTile - 2) * widthOfExtendedTile) + 2; // offset kde začíná haló zóna, kterou je třeba přijmout od spodního souseda

    int offsetOfHaloZoneToSendToLeft = widthOfExtendedTile * 2 + 2; // offset kde začíná haló zóna, kterou je třeba odeslat levému sousedovi
    int offsetOfHaloZoneToReceiveFromLeft = widthOfExtendedTile * 2;  // offset kde začíná haló zóna, kterou je třeba přijmout od levého souseda

    int offsetOfHaloZoneToSendToRight = widthOfExtendedTile * 3 - 4; // offset kde začíná haló zóna, kterou je třeba odeslat pravému sousedovi
    int offsetOfHaloZoneToReceiveFromRight = widthOfExtendedTile * 3 - 2; // offset kde začíná haló zóna, kterou je třeba přijmout od pravého souseda


    int requestIndex = 0; // index do pole requestu, který se bude s každou komunikací inkrementovat
    
    // výměna hodnot z haló zón se sousedy
    if (hasUpperNeighbor) {    
        MPI_Isend (&(extendedTileOfoldTemp[offsetOfHaloZoneToSendToUpper]), 1, twoFloatAdjacentRows, rankOfUpperNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests[requestIndex++]);
        MPI_Irecv (&(extendedTileOfoldTemp[offsetOfHaloZoneToReceiveFromUpper]), 1, twoFloatAdjacentRows, rankOfUpperNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests[requestIndex++]);
    
        MPI_Isend (&(extendedTileOfdomainMap[offsetOfHaloZoneToSendToUpper]), 1, twoIntAdjacentRows, rankOfUpperNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests[requestIndex++]);
        MPI_Irecv (&(extendedTileOfdomainMap[offsetOfHaloZoneToReceiveFromUpper]), 1, twoIntAdjacentRows, rankOfUpperNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests[requestIndex++]);
        
        MPI_Isend (&(extendedTileOfdomainParams[offsetOfHaloZoneToSendToUpper]), 1, twoFloatAdjacentRows, rankOfUpperNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests[requestIndex++]);
        MPI_Irecv (&(extendedTileOfdomainParams[offsetOfHaloZoneToReceiveFromUpper]), 1, twoFloatAdjacentRows, rankOfUpperNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests[requestIndex++]);
    }
    if (hasBottomNeighbor) {
        MPI_Isend (&(extendedTileOfoldTemp[offsetOfHaloZoneToSendToBottom]), 1, twoFloatAdjacentRows, rankOfBottomNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests[requestIndex++]);
        MPI_Irecv (&(extendedTileOfoldTemp[offsetOfHaloZoneToReceiveFromBottom]), 1, twoFloatAdjacentRows, rankOfBottomNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests[requestIndex++]);
    
        MPI_Isend (&(extendedTileOfdomainMap[offsetOfHaloZoneToSendToBottom]), 1, twoIntAdjacentRows, rankOfBottomNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests[requestIndex++]);
        MPI_Irecv (&(extendedTileOfdomainMap[offsetOfHaloZoneToReceiveFromBottom]), 1, twoIntAdjacentRows, rankOfBottomNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests[requestIndex++]);
        
        MPI_Isend (&(extendedTileOfdomainParams[offsetOfHaloZoneToSendToBottom]), 1, twoFloatAdjacentRows, rankOfBottomNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests[requestIndex++]);
        MPI_Irecv (&(extendedTileOfdomainParams[offsetOfHaloZoneToReceiveFromBottom]), 1, twoFloatAdjacentRows, rankOfBottomNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests[requestIndex++]);
    }
    if (hasLeftNeighbor) {
        MPI_Isend (&(extendedTileOfoldTemp[offsetOfHaloZoneToSendToLeft]), 1, twoFloatAdjacentColumns, rankOfLeftNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests[requestIndex++]);
        MPI_Irecv (&(extendedTileOfoldTemp[offsetOfHaloZoneToReceiveFromLeft]), 1, twoFloatAdjacentColumns, rankOfLeftNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests[requestIndex++]);
    
        MPI_Isend (&(extendedTileOfdomainMap[offsetOfHaloZoneToSendToLeft]), 1, twoIntAdjacentColumns, rankOfLeftNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests[requestIndex++]);
        MPI_Irecv (&(extendedTileOfdomainMap[offsetOfHaloZoneToReceiveFromLeft]), 1, twoIntAdjacentColumns, rankOfLeftNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests[requestIndex++]);
        
        MPI_Isend (&(extendedTileOfdomainParams[offsetOfHaloZoneToSendToLeft]), 1, twoFloatAdjacentColumns, rankOfLeftNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests[requestIndex++]);
        MPI_Irecv (&(extendedTileOfdomainParams[offsetOfHaloZoneToReceiveFromLeft]), 1, twoFloatAdjacentColumns, rankOfLeftNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests[requestIndex++]);
    }
    if (hasRightNeighbor) {
        MPI_Isend (&(extendedTileOfoldTemp[offsetOfHaloZoneToSendToRight]), 1, twoFloatAdjacentColumns, rankOfRightNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests[requestIndex++]);
        MPI_Irecv (&(extendedTileOfoldTemp[offsetOfHaloZoneToReceiveFromRight]), 1, twoFloatAdjacentColumns, rankOfRightNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests[requestIndex++]);
    
        MPI_Isend (&(extendedTileOfdomainMap[offsetOfHaloZoneToSendToRight]), 1, twoIntAdjacentColumns, rankOfRightNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests[requestIndex++]);
        MPI_Irecv (&(extendedTileOfdomainMap[offsetOfHaloZoneToReceiveFromRight]), 1, twoIntAdjacentColumns, rankOfRightNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests[requestIndex++]);
        
        MPI_Isend (&(extendedTileOfdomainParams[offsetOfHaloZoneToSendToRight]), 1, twoFloatAdjacentColumns, rankOfRightNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests[requestIndex++]);
        MPI_Irecv (&(extendedTileOfdomainParams[offsetOfHaloZoneToReceiveFromRight]), 1, twoFloatAdjacentColumns, rankOfRightNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests[requestIndex++]);
    }
    
    // počkáme až se všechny zprávy vymění za účelem výměny halo zón
    MPI_Waitall(numOfRequest, array_of_requests, array_of_statuses);  
    
    // hodnoty z haló zón oldTemp překopírujeme ještě do newTemp, aby byly před samotným výpočtem tyto hodnoty synchronizované, jinak bychom se později dočkali zajímavých výsledků
    // firstprivatizujeme cokoliv jen jde kvůli výkonu přístupu k daným proměnným repsketive ukazatelům
    #pragma omp parallel for simd default(none) firstprivate(heightOfExtendedTile, widthOfExtendedTile, extendedTileOfoldTemp, extendedTileOfnewTemp)
    for (size_t i = 0; i < widthOfExtendedTile * heightOfExtendedTile; i++) {
        extendedTileOfnewTemp[i] = extendedTileOfoldTemp[i];
    }
    
    
    /*************** definujmee vyseknutí originální dlažice z rozšířené matice - bude potřeba při posílání masterovi *********/            
        
    // vyseknutý typ dlaždice z pole extendedTileOfnewTemp - tedy získáme vlastní obsah dlaždice bez haló zón    
    MPI_Datatype originTileOfnewTempType; 

    // pomocné pole pro získání originální dlaždice z rozšířené dlaždice
    const int array_of_sizes2[2] = {heightOfExtendedTile, widthOfExtendedTile};   
    const int array_of_subsizes2[2] = {heightOfTile, widthOfTile};   
    const int array_of_starts2[2] = {2, 2};
    
    // dlaždice pole extendedTileOfnewTemp, kterou z tohoto pole vysekáváme a získáváme tak originální matici, která patří danému procesu
    MPI_Type_create_subarray(2, array_of_sizes2, array_of_subsizes2, array_of_starts2, MPI_ORDER_C, MPI_FLOAT, &originTileOfnewTempType);
    MPI_Type_commit(&originTileOfnewTempType);
    
    /*************** JÁDRO CELÉHO PROGRAMU - PARALELNÍ ITERATIVNÍ VÝPOČET *********/            
    
    bool containMiddleCol = false; // příznak jestli daná dlaždice ktrou má proces na starost obsahuje střední sloupec domény
    // nastavíme příznak containMiddleCol pro danou dlaždici/proces
    if  ((widthOfTile * (rank % numOfTilesPerRow) <= edgeSize/2) && (edgeSize/2 < widthOfTile * (1 + (rank % numOfTilesPerRow)))) {
        containMiddleCol = true;
    }
    // offset od levého okraje dané dlaždice k prostřednímu sloupci který obsahuje (jen pro dlaždice které obsahují daný slopec má tento výpočet smysl)
    int offsetToMiddleCol = edgeSize/2 - (rank % numOfTilesPerRow)*widthOfTile; // ofset dlaždice obsahující střední sloupec od jejího začátku k tomuto sloupci
    
    numOfRequest = 0; // kolik bude celkově použito reuestů pro point to point komunikaci, záleží kolik má dlaždice sousedů
    if (hasUpperNeighbor) numOfRequest += 2; // s každým sousedem si vyměníme celkově 2 zprávy = 2 requesty
    if (hasBottomNeighbor) numOfRequest += 2;
    if (hasLeftNeighbor) numOfRequest += 2;
    if (hasRightNeighbor) numOfRequest += 2;
    
    MPI_Request array_of_requests2[numOfRequest]; // pole requestů
    MPI_Status array_of_statuses2[numOfRequest]; // pole statusů
    
        
    // podle toho jestli ma dlazdice sousedy se vypocitaji prislusne ofsety (okraje, mantinely ve kterých se při výpočtu krajních bodů pohybovat), aby se nepocitali krajni body domeny pri vypoctu krajnich bodu, ktere se zaslou sousedum jako halo zony
    int topOffset = 2;
    int bottomOffset = 2;
    int leftOffset = 2;
    int rightOffset = 2;
    
    if (!hasUpperNeighbor) {
        topOffset += 2;
    }
    if (!hasBottomNeighbor) {
        bottomOffset += 2;
    }
    if (!hasLeftNeighbor) {
        leftOffset += 2;
    }
    if (!hasRightNeighbor) {
        rightOffset += 2;
    }  
    
   
  if (rank == 0 && !batchMode) {
       printf("Starting parallel simulation... \n");
  }
  
  //---------------------- [5] start the stop watch ------------------------------//
  double elapsedTime;
  // ubehnutý čas měří proces s rankem 0
  if (rank == 0) {
    elapsedTime = MPI_Wtime();
  }
  size_t i, j;
  size_t iteration;
  float middleColAvgTemp = 0.0f; // průměrná teplota prostředního sloupce domény
  float middleColTempSum = 0.0f; // dílčí součet hodnot prostředního sloupce pro danou dlaždici která jej obsahuje
  size_t printCounter = 1;

  // [6] Start the iterative simulation
    // co jde firstzprivatizovat, tak to firstprivatizujeme (kromě nějakých složitějších datovfých typů), abycjhom zvýšili výkon při přístupu k daným proměnným, respektive ukazatelům, ukazatel na newTemp a oldTemp samozřejmě nemůžeme firstPrivatizovat protože je swapujeme (a chceme aby to bylo vidět emzi všemi vlákny) a potřebujeme je tedy mít shared
    #pragma omp parallel default(none) shared(middleColTempSum, twoFloatAdjacentRows, twoFloatAdjacentColumns, sendcounts, displs, array_of_requests2, array_of_statuses2, tileOfInitTempType, originTileOfnewTempType, extendedTileOfnewTemp, extendedTileOfoldTemp) firstprivate(parResult, extendedTileOfdomainMap, extendedTileOfdomainParams, hasRightNeighbor, hasBottomNeighbor, hasUpperNeighbor, hasLeftNeighbor, heightOfTile, widthOfTile, numOfRequest, TAG, offsetToMiddleCol, useParallelIO, batchMode, numOfTilesPerCol, numOfTilesPerRow, diskWriteIntensity, rank, printCounter, file_id, middleColAvgTemp, edgeSize, containMiddleCol, requestIndex, rankOfRightNeighbor, rankOfLeftNeighbor, rankOfBottomNeighbor, rankOfUpperNeighbor, offsetOfHaloZoneToReceiveFromRight, offsetOfHaloZoneToSendToRight, offsetOfHaloZoneToReceiveFromLeft, offsetOfHaloZoneToSendToLeft, offsetOfHaloZoneToReceiveFromBottom, offsetOfHaloZoneToSendToBottom, offsetOfHaloZoneToReceiveFromUpper, offsetOfHaloZoneToSendToUpper, airFlowRate, coolerTemp, nIterations, topOffset,bottomOffset, leftOffset, rightOffset, heightOfExtendedTile, widthOfExtendedTile) private(iteration, i, j) 
    {
      for (iteration = 0; iteration < nIterations; iteration++)
      {
        
        // spocitam okraje dlazdice (pokud to neni okraj domeny), jdu v dlaždici po řádcích a vnitřek matice přeskakuji, po řádcích jdu proto abych redukoval cache miss, ktere by bylo velke kdybych sel po sloupcich
        // TAKTO NAPSANÉ TOTO BOHUŽEL NELZE PARALELIZOVAT (S ROZDELENIM PRACE) POMOCI TYMU VLAKEN OPENMP, MUSEL BYCH TO PŘEPSAT, NADRUHOU STRANU TO SE MI NECHCE, PROTOZE KDYBYCH PROCHAZEL DLAZDICI PO SLOUPCICH, TAK BY NEJSPISE DOCHAZELO K CASTEMU CACHE MISS (JAK JIZ BYLO DRIVE RECENO), JO TAKZE TEN VYKON BY SEL STEJNE DO HAJE   
        // kdyz nejde rozdelit prace teto smycky, tak ji bude provadet jen jedno vlakno, neni mozne aby se to provadelo paralelne vsemi vlakny 
        #pragma omp single 
        {
            for (i = topOffset; i < heightOfExtendedTile - bottomOffset; i++) {
                if (i >= 4 && i < heightOfExtendedTile - 4) {
                    for (j = leftOffset; j < widthOfExtendedTile - rightOffset; j++) {
                        if (j == 4) { 
                            j = widthOfExtendedTile - 5;
                            continue;
                        } 
                        ComputePoint(extendedTileOfoldTemp,
                                     extendedTileOfnewTemp,
                                     extendedTileOfdomainParams,
                                     extendedTileOfdomainMap,
                                     i, j,
                                     widthOfExtendedTile, 
                                     airFlowRate,
                                     coolerTemp);
                    }
                }
                else {
                    for (j = leftOffset; j < widthOfExtendedTile - rightOffset; j++) {
                        ComputePoint(extendedTileOfoldTemp,
                                     extendedTileOfnewTemp,
                                     extendedTileOfdomainParams,
                                     extendedTileOfdomainMap,
                                     i, j,
                                     widthOfExtendedTile, 
                                     airFlowRate,
                                     coolerTemp);
                    }
                }
            }
        }
        // předpokládáme FUNNLEED thead safety, musíme použít mastera
        #pragma omp master 
        {
          
            // ZAŠLU SPOČÍTANÉ OKRAJE  A PŘIJMU OKRAJE OD SOUSEDŮ
            requestIndex = 0; // index do pole requestu, který se bude s každou komunikací inkrementovat
            
            // výměna hodnot z haló zón se sousedy
            if (hasUpperNeighbor) {
                MPI_Isend (&(extendedTileOfnewTemp[offsetOfHaloZoneToSendToUpper]), 1, twoFloatAdjacentRows, rankOfUpperNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests2[requestIndex++]);
                MPI_Irecv (&(extendedTileOfnewTemp[offsetOfHaloZoneToReceiveFromUpper]), 1, twoFloatAdjacentRows, rankOfUpperNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests2[requestIndex++]);
            
            }
            if (hasBottomNeighbor) {
                MPI_Isend (&(extendedTileOfnewTemp[offsetOfHaloZoneToSendToBottom]), 1, twoFloatAdjacentRows, rankOfBottomNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests2[requestIndex++]);
                MPI_Irecv (&(extendedTileOfnewTemp[offsetOfHaloZoneToReceiveFromBottom]), 1, twoFloatAdjacentRows, rankOfBottomNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests2[requestIndex++]);
            
            }
            if (hasLeftNeighbor) {
                MPI_Isend (&(extendedTileOfnewTemp[offsetOfHaloZoneToSendToLeft]), 1, twoFloatAdjacentColumns, rankOfLeftNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests2[requestIndex++]);
                MPI_Irecv (&(extendedTileOfnewTemp[offsetOfHaloZoneToReceiveFromLeft]), 1, twoFloatAdjacentColumns, rankOfLeftNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests2[requestIndex++]);
            
            }
            if (hasRightNeighbor) {
                MPI_Isend (&(extendedTileOfnewTemp[offsetOfHaloZoneToSendToRight]), 1, twoFloatAdjacentColumns, rankOfRightNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests2[requestIndex++]);
                MPI_Irecv (&(extendedTileOfnewTemp[offsetOfHaloZoneToReceiveFromRight]), 1, twoFloatAdjacentColumns, rankOfRightNeighbor, TAG, MPI_COMM_WORLD, &array_of_requests2[requestIndex++]);
            
            }  
        }
        // zde baroeru nepotřebujeme, necekame ze se zemnila hodnota sdilych promennych
          
        // [a] calculate one iteration of the heat distribution 
        #pragma omp for simd
        for (i = 4; i < heightOfExtendedTile - 4; i++) {
          for (j = 4; j < widthOfExtendedTile - 4; j++) {
            ComputePoint(extendedTileOfoldTemp,
                         extendedTileOfnewTemp,
                         extendedTileOfdomainParams,
                         extendedTileOfdomainMap,
                         i, j,
                         widthOfExtendedTile, 
                         airFlowRate,
                         coolerTemp);
                         //continue;
          }
        
        }
        // [b] Compute the average temperature in the middle column, nepotřebujeme speciální komunikátor, protože každý proces zašle skrze reduce nějaké číslo, ovšem jen procesy, které drží dlaždice které obsahují prostřední sloupec zašlou číslo různé od 0.0f vyjadřující dílčí sumu hodnot prostředního sloupce
        // s proměnnou middleColAvgTemp pracuje pouze master vlakno
        #pragma omp master 
        middleColAvgTemp = 0.0f;
        
        // s proměnnou middleColTempSum pracují všechna vlakna, ale jen jedno to musi inicializovat
        #pragma omp single
        middleColTempSum = 0.0f;
        
        // poouze pokud dlaždice obsahuje střední sloupec, tak se sečtou hodnoty v tomto prostředním sloupci, které dlaždice obsahuje
        if (containMiddleCol) {
           // provedem lokalni redukci hodnot stredniho sloupce v ramci dlazdice, nasledne tuto lokalni redukci jeste zredukujem pres vsechyn provesy
           #pragma omp for simd reduction (+:middleColTempSum)
           for (int p = 2; p <= heightOfTile + 1; p++) {
                middleColTempSum += extendedTileOfnewTemp[p*widthOfExtendedTile + 2 + offsetToMiddleCol];
            } 
        }
        
        // tahle sekce se uz musi provadet masterem, stejne jako u prijektu 1, navic je zde kolektivni komunikace, takze by to ani jinak neslo pokud uvazujeme FUNNELED thread safety
        #pragma omp master
        {
            
            // kolektivní komunikace neintereferuje s point to point komuniací v rámci stejného komunikátoru, sečteme dílčí součiny hodnot středního sloupce a dostaneme celkovou sumu hodnot středního sloupce
            MPI_Reduce(&middleColTempSum, &middleColAvgTemp, 1, MPI_FLOAT, MPI_SUM, 0, MPI_COMM_WORLD); // commMiddleCol
            // mě z nějakého neznámého důvodů - (fakt neznámého - ladil jsem to tak den (škoda jednoho krásného dne, který i když uplynul zůstane navždy v našich srdcích), než jsem zkusil, aby následující řádek provedli všechny procesy, nejen proces s rankem 0) toto musí provést všechny procesy jinak se ten program prpostě zaskene, neptejete se mě proč, podle mě je v tom nějaká černá magie, prostě ta podmínak if (rank == 0) to normálně blokne ten program...Naštstí tím výkon nějak moc neztrácíme v podattaě to provedou všechny proces paralleně i když smaozřejmě tošku zbytečně stačilo by kdyby to provedl proces 0
            middleColAvgTemp /= edgeSize;
                      
            // počkáme až se všechny zprávy vymění za účelem výměny halo zón
            MPI_Waitall(numOfRequest, array_of_requests2, array_of_statuses2); 

            // [c] Store time step in the output file if necessary
            if (iteration % diskWriteIntensity == 0)
            {
                if(!useParallelIO)
                {
                    // Serial I/O
                    // store data to root
                    // *** Zde posbirejte data do 0. procesu, ktery vytvarel vystupni soubor ***
                    MPI_Gatherv(extendedTileOfnewTemp, 1, originTileOfnewTempType, 
                                parResult, sendcounts, displs, tileOfInitTempType, 0, MPI_COMM_WORLD);


                    // store time step in the output file if necessary
                    if (rank == 0 && file_id != H5I_INVALID_HID)
                    {
                        StoreDataIntoFile(file_id,
                                          parResult,
                                          edgeSize,
                                          iteration / diskWriteIntensity, 
                                          iteration);
                    }
                }
                else
                {
                    // Parallel I/O
                    if(file_id != H5I_INVALID_HID)
                    {
                        StoreDataIntoFileParallel(file_id,
                                                  extendedTileOfnewTemp,
                                                  edgeSize,
                                                  widthOfExtendedTile, heightOfExtendedTile,
                                                  (widthOfExtendedTile - 4) * (rank % numOfTilesPerRow), (heightOfExtendedTile - 4) * (rank / numOfTilesPerCol),
                                                  iteration / diskWriteIntensity, iteration);
                    }
                }  
            }

            // [d] Swap new and old values
            swap(extendedTileOfnewTemp, extendedTileOfoldTemp);
            
            // [e] Print progress and average temperature of the middle column
            if (rank == 0 && ((float)(iteration) >= (nIterations-1) / 10.0f * (float)printCounter) 
                && !batchMode)
            {
              printf("Progress %ld%% (Average Temperature %.2f degrees)\n", 
                     (iteration+1) * 100L / (nIterations), 
                     middleColAvgTemp);
              ++printCounter;
            }
        }
        // sesynchronizujem tym vlaken, aby vsechna vlakna videli swapnute extendedTileOfnewTemp a extendedTileOfoldTemp
        #pragma omp barrier

      } // for iteration    
    } // pragma parallel
  
    // *** Zde posbíráme data do 0. procesu ***
    MPI_Gatherv(extendedTileOfnewTemp, 1,  originTileOfnewTempType, 
                parResult, sendcounts, displs, tileOfInitTempType, 0, MPI_COMM_WORLD);
  
  //-------------------- stop the stop watch  --------------------------------//  
    if (rank == 0) {
      double totalTime = MPI_Wtime() - elapsedTime;

      // [7] Print final result
      if (!batchMode)
        printf("\nExecution time of parallel version %.5f\n", totalTime);
      else
        printf("%s;%s;%f;%e;%e\n", outputFileName.c_str(), "par",
                                   middleColAvgTemp, totalTime,
                                   totalTime / nIterations);   
    }
  
  // close the output file
  if (file_id != H5I_INVALID_HID) H5Fclose(file_id);
} // end of ParallelHeatDistribution
//------------------------------------------------------------------------------


/**
 * Store time step into output file (as a new dataset in Pixie format
 * @param [in] h5fileID   - handle to the output file
 * @param [in] data       - data to write
 * @param [in] edgeSize   - size of the domain
 * @param [in] snapshotId - snapshot id
 * @param [in] iteration  - id of iteration);
 */
void StoreDataIntoFile(hid_t         h5fileId,
                       const float  *data,
                       const size_t  edgeSize,
                       const size_t  snapshotId,
                       const size_t  iteration)
{
  hid_t   dataset_id, dataspace_id, group_id, attribute_id;
  hsize_t dims[2] = {edgeSize, edgeSize};

  string groupName = "Timestep_" + to_string((unsigned long long) snapshotId);

  // Create a group named "/Timestep_snapshotId" in the file.
  group_id = H5Gcreate(h5fileId,
                       groupName.c_str(),
                       H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);


  // Create the data space. (2D matrix)
  dataspace_id = H5Screate_simple(2, dims, NULL);

  // create a dataset for temperature and write data
  string datasetName = "Temperature";
  dataset_id = H5Dcreate(group_id,
                         datasetName.c_str(),
                         H5T_NATIVE_FLOAT,
                         dataspace_id,
                         H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  H5Dwrite(dataset_id,
           H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT,
           data);

  // close dataset
  H5Sclose(dataspace_id);


  // write attribute
  string atributeName="Time";
  dataspace_id = H5Screate(H5S_SCALAR);
  attribute_id = H5Acreate2(group_id, atributeName.c_str(),
                            H5T_IEEE_F64LE, dataspace_id,
                            H5P_DEFAULT, H5P_DEFAULT);

  double snapshotTime = double(iteration);
  H5Awrite(attribute_id, H5T_IEEE_F64LE, &snapshotTime);
  H5Aclose(attribute_id);


  // Close the dataspace.
  H5Sclose(dataspace_id);

  // Close to the dataset.
  H5Dclose(dataset_id);

  // Close the group.
  H5Gclose(group_id);
} // end of StoreDataIntoFile
//------------------------------------------------------------------------------

/**
 * Store time step into output file using parallel version of HDF5
 * @param [in] h5fileId   - handle to the output file
 * @param [in] data       - data to write
 * @param [in] edgeSize   - size of the domain
 * @param [in] tileWidth  - width of the tile
 * @param [in] tileHeight - height of the tile
 * @param [in] tilePosX   - position of the tile in the grid (X-dir)
 * @param [in] tilePosY   - position of the tile in the grid (Y-dir)
 * @param [in] snapshotId - snapshot id
 * @param [in] iteration  - id of iteration
 */
void StoreDataIntoFileParallel(hid_t h5fileId,
                               const float *data,
                               const size_t edgeSize,
                               const size_t tileWidth, const size_t tileHeight,
                               const size_t tilePosX, const size_t tilePosY,
                               const size_t snapshotId,
                               const size_t iteration)
{
    hid_t dataset_id, dataspace_id, group_id, attribute_id, memspace_id;
    const hsize_t dims[2] = { edgeSize, edgeSize };
    const hsize_t offset[2] = { tilePosY, tilePosX };
    const hsize_t tile_dims[2] = { tileHeight, tileWidth };
    const hsize_t core_dims[2] = { tileHeight - 4, tileWidth - 4 };
    const hsize_t core_offset[2] = { 2, 2 };

    string groupName = "Timestep_" + to_string((unsigned long)snapshotId);

    // Create a group named "/Timestep_snapshotId" in the file.
    group_id = H5Gcreate(h5fileId,
                         groupName.c_str(),
                         H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    // Create the data space in the output file. (2D matrix)
    dataspace_id = H5Screate_simple(2, dims, NULL);

    // create a dataset for temperature and write data
    string datasetName = "Temperature";
    dataset_id = H5Dcreate(group_id,
                           datasetName.c_str(),
                           H5T_NATIVE_FLOAT,
                           dataspace_id,
                           H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    // create the data space in memory representing local tile. (2D matrix)
    memspace_id = H5Screate_simple(2, tile_dims, NULL);

    // select appropriate block of the local tile. (without halo zones)
    H5Sselect_hyperslab(memspace_id, H5S_SELECT_SET, core_offset, NULL, core_dims, NULL);

    // select appropriate block of the output file, where local tile will be placed.
    H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset, NULL, core_dims, NULL);

    // setup collective write using MPI parallel I/O
    hid_t hPropList = H5Pcreate(H5P_DATASET_XFER);
    H5Pset_dxpl_mpio(hPropList, H5FD_MPIO_COLLECTIVE);

    H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, memspace_id, dataspace_id, hPropList, data);

    // close memory spaces and property list
    H5Sclose(memspace_id);
    H5Sclose(dataspace_id);
    H5Pclose(hPropList);

    // write attribute
    string attributeName = "Time";
    dataspace_id = H5Screate(H5S_SCALAR);
    attribute_id = H5Acreate2(group_id, attributeName.c_str(),
                              H5T_IEEE_F64LE, dataspace_id,
                              H5P_DEFAULT, H5P_DEFAULT);

    double snapshotTime = double(iteration);
    H5Awrite(attribute_id, H5T_IEEE_F64LE, &snapshotTime);
    H5Aclose(attribute_id);

    // close the dataspace
    H5Sclose(dataspace_id);

    // close the dataset and the group
    H5Dclose(dataset_id);
    H5Gclose(group_id);
}
//------------------------------------------------------------------------------

/**
 * Main function of the project
 * @param [in] argc
 * @param [in] argv
 * @return
 */
int main(int argc, char *argv[])
{
  int rank, size;

  ParseCommandline(argc, argv, parameters);

  // Initialize MPI
  MPI_Init(&argc, &argv);

  // Get MPI rank and size
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);


  if (rank == 0)
  {
    // Create material properties and load from file
    materialProperties.LoadMaterialData(parameters.materialFileName, true);
    parameters.edgeSize = materialProperties.edgeSize;

    parameters.PrintParameters();
  }
  else
  {
    // Create material properties and load from file
    materialProperties.LoadMaterialData(parameters.materialFileName, false);
    parameters.edgeSize = materialProperties.edgeSize;
  }

  if (parameters.edgeSize % size)
  {
    if (rank == 0)
      printf("ERROR: number of MPI processes is not a divisor of N\n");
    MPI_Abort(MPI_COMM_WORLD, 1);
  }

  if (parameters.IsRunSequntial())
  {
    if (rank == 0)
    {
      // Memory allocation for output matrices.
      seqResult = (float*)_mm_malloc(materialProperties.nGridPoints * sizeof(float), DATA_ALIGNMENT);

      SequentialHeatDistribution(seqResult,
                                 materialProperties,
                                 parameters,
                                 parameters.outputFileName);
    }
  }

  if (parameters.IsRunParallel())
  {
    // Memory allocation for output matrix.
    if (rank == 0)
      parResult = (float*) _mm_malloc(materialProperties.nGridPoints * sizeof(float), DATA_ALIGNMENT);
    else
      parResult = NULL;

    ParallelHeatDistribution(parResult,
                             materialProperties,
                             parameters,
                             parameters.outputFileName);
  }

  // Validate the outputs
  if (parameters.IsValidation() && rank == 0)
  {
    if (parameters.debugFlag)
    {
      printf("---------------- Sequential results ---------------\n");
      PrintArray(seqResult, materialProperties.edgeSize);

      printf("----------------- Parallel results ----------------\n");
      PrintArray(parResult, materialProperties.edgeSize);
    }

    if (VerifyResults(seqResult, parResult, parameters, 0.001f))
      printf("Verification OK\n");
    else
      printf("Verification FAILED\n");
  }

  /* Memory deallocation*/
  _mm_free(seqResult);
  _mm_free(parResult);

  MPI_Finalize();

  return EXIT_SUCCESS;
} // end of main
//------------------------------------------------------------------------------
