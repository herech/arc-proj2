#!/bin/bash

declare -a cores=(1 16 32 64 128)
index=0
output="out_mpi.txt"
input="run_full_mpi_out.csv"

#if file does exist
if [ -f "$output" ]
then
  rm $output
fi

printf ${cores[index]}';' >> $output

tail -n +2 $input | \
while read LINE
do
  if [ ! $(echo $LINE | cut -f 1 -d';') -eq $((${cores[index]})) ]
  then 
    index=$(($index+1))
    echo "" >> $output
    echo -n ${cores[index]}';' >> $output
  fi

  echo -n $(echo $LINE | cut -f 12 -d';')';' >> $output
done

pushd ../plots

./gnuplot arc_scaling_mpi.plot
./gnuplot arc_efficiency_mpi.plot
./gnuplot arc_speedup_mpi.plot

popd
