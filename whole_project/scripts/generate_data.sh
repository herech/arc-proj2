#!/bin/bash

ml intel/2018a HDF5/1.10.1-intel-2018a

# domain sizes
declare -a sizes=(256 512 1024 2048 4096)

# generate input files
for size in ${sizes[*]} 
do
  echo "Generating input data ${size}x${size}..."
  ../data_generator/arc_generator -o input_data_${size}.h5 -N ${size} -H 100 -C 20
done
