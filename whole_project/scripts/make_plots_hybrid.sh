#!/bin/bash

declare -a cores=(1 24 48 96 192)
index=0
output="out_hybrid.txt"
input="run_full_hybrid_out.csv"

#if file does exist
if [ -f "$output" ]
then
  rm $output
fi

printf ${cores[index]}';' >> $output

tail -n +2 $input | \
while read LINE
do
  if [ ! $(($(echo $LINE | cut -f 1 -d';') * $(echo $LINE | cut -f 2 -d';'))) -eq $((${cores[index]})) ]
  then 
    index=$(($index+1))
    echo "" >> $output
    echo -n ${cores[index]}';' >> $output
  fi

  echo -n $(echo $LINE | cut -f 12 -d';')';' >> $output
done

pushd ../plots

./gnuplot arc_scaling_hybrid.plot
./gnuplot arc_efficiency_hybrid.plot
./gnuplot arc_speedup_hybrid.plot

popd
