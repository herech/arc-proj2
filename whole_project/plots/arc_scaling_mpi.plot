#
# ARC 2017, VUT FIT
# Script generating Strong MPI scaling plot
# Authors: Vojtech Nikl (inikl@fit.vutbr.cz), 
#          Filip Vaverka (ivaverka@fit.vutbr.cz)
#
# Inputs: out_mpi.txt
# Output: arc_scaling_mpi.svg

reset

input = '../scripts/out_mpi.txt'

set term svg enhanced font 'Verdana,11'
set output 'arc_scaling_mpi.svg'
set border linewidth 1

# define axis
# remove border on top and right and set color to gray
set style line 11 lc rgb '#808080' lt 1
set border 3 back ls 11
set tics nomirror
#define grid
set style line 12 lc rgb '#808080' lt 0 lw 0.25
set grid back ls 12

set datafile separator ";"

set xlabel 'Number of cores'
set ylabel 'Iteration time [ms]'
set key outside right center

set logscale x 2
set logscale y 2

set format y "%g"
set xtics (1, 16, 32, 64, 128) out
set ytics out

set title "Strong scaling MPI"

set key invert
set key reverse
set key Left

set size square 1,1

plot input\
	   using 1:($2*1000) with linespoints title '256^2 without I/O' lc rgb "#9400D3" pt 8 ps 0.75 dt 3,\
        '' using 1:($3*1000) with linespoints title '256^2 with seq I/O' lc rgb "#9400D3" pt 9 ps 0.75 dt "_",\
	'' using 1:($4*1000) with linespoints title '256^2 with par I/O' lc rgb "#9400D3" pt 9 ps 0.75,\
	'' using 1:($5*1000) with linespoints title '512^2 without I/O' lc rgb "blue" pt 6 ps 0.75 dt 3,\
	'' using 1:($6*1000) with linespoints title '512^2 with seq I/O' lc rgb "blue" pt 7 ps 0.75 dt "_",\
	'' using 1:($7*1000) with linespoints title '512^2 with par I/O' lc rgb "blue" pt 7 ps 0.75,\
	'' using 1:($8*1000) with linespoints title '1024^2 without I/O' lc rgb "#006400" pt 12 ps 0.75 dt 3,\
	'' using 1:($9*1000) with linespoints title '1024^2 with seq I/O' lc rgb "#006400" pt 13 ps 0.75 dt "_",\
	'' using 1:($10*1000) with linespoints title '1024^2 with par I/O' lc rgb "#006400" pt 13 ps 0.75,\
	'' using 1:($11*1000) with linespoints title '2048^2 without I/O' lc rgb "#FF8C00" pt 4 ps 0.75 dt 3,\
	'' using 1:($12*1000) with linespoints title '2048^2 with seq I/O' lc rgb "#FF8C00" pt 5 ps 0.75 dt "_",\
	'' using 1:($13*1000) with linespoints title '2048^2 with par I/O' lc rgb "#FF8C00" pt 5 ps 0.75,\
	'' using 1:($14*1000) with linespoints title '4096^2 without I/O' lc rgb "red" pt 11 ps 0.75 dt 3,\
	'' using 1:($15*1000) with linespoints title '4096^2 with seq I/O' lc rgb "red" pt 10 ps 0.75 dt "_",\
	'' using 1:($16*1000) with linespoints title '4096^2 with par I/O' lc rgb "red" pt 11 ps 0.75
