#
# ARC 2017, VUT FIT
# Script generating Strong scaling hybrid plot
# Authors: Vojtech Nikl (inikl@fit.vutbr.cz), 
#          Filip Vaverka (ivaverka@fit.vutbr.cz)
#
# Inputs: out_hybrid.txt
# Output: arc_speedup_hybrid.svg

reset

input = '../scripts/out_hybrid.txt'

set term svg enhanced font 'Verdana,11'
set output 'arc_speedup_hybrid.svg'
set border linewidth 1

# define axis
# remove border on top and right and set color to gray
set style line 11 lc rgb '#808080' lt 1
set border 3 back ls 11
set tics nomirror
#define grid
set style line 12 lc rgb '#808080' lt 0 lw 0.25
set grid back ls 12

set datafile separator ";"

set xlabel 'Number of cores'
set ylabel 'Speedup'
set key outside right center

stats input every ::0::0 using 3 nooutput; seq_256_it_time = STATS_min
stats input every ::0::0 using 6 nooutput; seq_512_it_time = STATS_min
stats input every ::0::0 using 9 nooutput; seq_1024_it_time = STATS_min
stats input every ::0::0 using 12 nooutput; seq_2048_it_time = STATS_min
stats input every ::0::0 using 15 nooutput; seq_4096_it_time = STATS_min

#set logscale x 2
#set logscale y 2

set format y "%g"
set xtics (1, 24, 48, 96, 192) out
set ytics out

set title "Speedup hybrid"

set key invert
set key reverse
set key Left

set size square 1,1

plot input\
           using 1:(seq_256_it_time/$2) with linespoints title '256^2 without I/O' lc rgb "#9400D3" pt 8 ps 0.75 dt 3,\
	'' using 1:(seq_256_it_time/$3) with linespoints title '256^2 with seq I/O' lc rgb "#9400D3" pt 9 ps 0.75 dt "_",\
	'' using 1:(seq_256_it_time/$4) with linespoints title '256^2 with par I/O' lc rgb "#9400D3" pt 9 ps 0.75,\
	'' using 1:(seq_512_it_time/$5) with linespoints title '512^2 without I/O' lc rgb "blue" pt 6 ps 0.75 dt 3,\
	'' using 1:(seq_512_it_time/$6) with linespoints title '512^2 with seq I/O' lc rgb "blue" pt 7 ps 0.75 dt "_",\
	'' using 1:(seq_512_it_time/$7) with linespoints title '512^2 with par I/O' lc rgb "blue" pt 7 ps 0.75,\
	'' using 1:(seq_1024_it_time/$8) with linespoints title '1024^2 without I/O' lc rgb "#006400" pt 12 ps 0.75 dt 3,\
	'' using 1:(seq_1024_it_time/$9) with linespoints title '1024^2 with seq I/O' lc rgb "#006400" pt 13 ps 0.75 dt "_",\
	'' using 1:(seq_1024_it_time/$10) with linespoints title '1024^2 with par I/O' lc rgb "#006400" pt 13 ps 0.75,\
	'' using 1:(seq_2048_it_time/$11) with linespoints title '2048^2 without I/O' lc rgb "#FF8C00" pt 4 ps 0.75 dt 3,\
	'' using 1:(seq_2048_it_time/$12) with linespoints title '2048^2 with seq I/O' lc rgb "#FF8C00" pt 5 ps 0.75 dt "_",\
	'' using 1:(seq_2048_it_time/$13) with linespoints title '2048^2 with par I/O' lc rgb "#FF8C00" pt 5 ps 0.75,\
	'' using 1:(seq_4096_it_time/$14) with linespoints title '4096^2 without I/O' lc rgb "red" pt 10 ps 0.75 dt 3,\
	'' using 1:(seq_4096_it_time/$15) with linespoints title '4096^2 with seq I/O' lc rgb "red" pt 11 ps 0.75 dt "_", \
	'' using 1:(seq_4096_it_time/$16) with linespoints title '4096^2 with par I/O' lc rgb "red" pt 11 ps 0.75
